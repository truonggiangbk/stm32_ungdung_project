#include "main.h"
#include "lcd.h"

extern TIM_HandleTypeDef htim2;

void delay_us(uint16_t us)
{
	__HAL_TIM_SET_COUNTER(&htim2, 0);
	while(__HAL_TIM_GET_COUNTER(&htim2) < us);
}

void send_to_lcd(char data, int rs)
{
	HAL_GPIO_WritePin(RS_GPIO_Port, RS_Pin, rs); 
	// 1 1 1 1 0 0 0 0
	HAL_GPIO_WritePin(D7_GPIO_Port, D7_Pin, (data >> 7)&0x01);
	HAL_GPIO_WritePin(D6_GPIO_Port, D6_Pin, (data >> 6)&0x01);
	HAL_GPIO_WritePin(D5_GPIO_Port, D5_Pin, (data >> 5)&0x01);
	HAL_GPIO_WritePin(D4_GPIO_Port, D4_Pin, (data >> 4)&0x01);
	
	HAL_GPIO_WritePin(E_GPIO_Port, E_Pin, 1);
	delay_us(20);
	HAL_GPIO_WritePin(E_GPIO_Port, E_Pin, 0);
	delay_us(20);
	
	HAL_GPIO_WritePin(D7_GPIO_Port, D7_Pin, (data >> 3)&0x01);
	HAL_GPIO_WritePin(D6_GPIO_Port, D6_Pin, (data >> 2)&0x01);
	HAL_GPIO_WritePin(D5_GPIO_Port, D5_Pin, (data >> 1)&0x01);
	HAL_GPIO_WritePin(D4_GPIO_Port, D4_Pin, (data >> 0)&0x01);
	
	HAL_GPIO_WritePin(E_GPIO_Port, E_Pin, 1);
	delay_us(20);
	HAL_GPIO_WritePin(E_GPIO_Port, E_Pin, 0);
	delay_us(20);	
}

void lcd_send_cmd (char cmd)
{
	send_to_lcd(cmd, 0);
}

void lcd_send_data (char data)
{
	send_to_lcd(data, 1);
}

void lcd_init_4bit(void)
{
    // 4 bit initialisation
    HAL_Delay(50);  // wait for >40ms
    lcd_send_cmd (0x30);
    HAL_Delay(5);  // wait for >4.1ms
    lcd_send_cmd (0x30);
    HAL_Delay(1);  // wait for >100us
    lcd_send_cmd (0x30);
    HAL_Delay(10);
    lcd_send_cmd (0x20);  // 4bit mode
    HAL_Delay(10);

  // dislay initialisation
    lcd_send_cmd (0x28); // Function set --> DL=0 (4 bit mode), N = 1 (2 line display) F = 0 (5x8 characters)
    HAL_Delay(1);
    lcd_send_cmd (0x08); //Display on/off control --> D=0,C=0, B=0  ---> display off
    HAL_Delay(1);
    lcd_send_cmd (0x01);  // clear display
    HAL_Delay(1);
    HAL_Delay(1);
    lcd_send_cmd (0x06); //Entry mode set --> I/D = 1 (increment cursor) & S = 0 (no shift)
    HAL_Delay(1);
    lcd_send_cmd (0x0C); //Display on/off control --> D = 1, C and B = 0. (Cursor and blink, last two bits)	
}

void lcd_clear(void)
{
	lcd_send_cmd(0x01);
	HAL_Delay(3);
}

void lcd_print_char(char ch)
{
	lcd_send_data(ch);
}

// "deviot.vn"
void lcd_print_str (char *str)
{
	while (*str) 
	{
		lcd_send_data (*str++);
	}
}

//row: 0,1
//col: 0 -> 15

void lcd_put_cur(int row, int col)
{
    switch (row)
    {
        case 0:
            col |= 0x80;
            break;
        case 1:
            col |= 0xC0;
            break;
    }
    lcd_send_cmd (col);
}

/**
 * @brief Shift content to right
 */
void lcd_shiftRight(uint8_t offset)
{
  for(uint8_t i=0; i<offset;i++)
  {
    lcd_send_cmd(0x1c);
  }
}

/**
 * @brief Shift content to left
 */
void lcd_shiftLeft(uint8_t offset)
{
  for(uint8_t i=0; i<offset;i++)
  {
    lcd_send_cmd(0x18);
  }
}


