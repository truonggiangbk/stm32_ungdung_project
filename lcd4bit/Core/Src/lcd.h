#ifndef __LCD_H
#define __LCD_H

#include <stdint.h>
void lcd_init_4bit(void);
void lcd_put_cur(int row, int col);
void lcd_print_char(char ch);
void lcd_print_str (char *str);
void lcd_shiftRight(uint8_t offset);
void lcd_shiftLeft(uint8_t offset);
void lcd_clear(void);
#endif