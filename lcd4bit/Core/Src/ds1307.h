#ifndef __DS1307_H
#define __DS1307_H

#include <stdint.h>

#define DS1307_I2C_ADDR 	0x68
#define DS1307_REG_SECOND 	0x00
#define DS1307_REG_MINUTE 	0x01
#define DS1307_REG_HOUR  	0x02
#define DS1307_REG_DOW    	0x03
#define DS1307_REG_DATE   	0x04
#define DS1307_REG_MONTH  	0x05
#define DS1307_REG_YEAR   	0x06
#define DS1307_REG_CONTROL 	0x07
#define DS1307_REG_UTC_HR	0x08
#define DS1307_REG_UTC_MIN	0x09
#define DS1307_REG_CENT    	0x10
#define DS1307_REG_RAM   	0x11
#define DS1307_TIMEOUT		1000

typedef struct{
   uint8_t sec;                       
   uint8_t min;
   uint8_t hour;
   uint8_t day;
   uint8_t date;
   uint8_t month;
   uint8_t year;
} ds1307_t;

uint8_t DS1307_Init(void);

uint8_t DS1307_GetSecond(void);
uint8_t DS1307_GetMinute(void);
uint8_t DS1307_GetHour(void);
uint8_t DS1307_GetDayOfWeek(void);
uint8_t DS1307_GetDate(void);
uint8_t DS1307_GetMonth(void);
uint8_t DS1307_GetYear(void);

void DS1307_SetClockHalt(uint8_t halt);
void DS1307_SetSecond(uint8_t second);
void DS1307_SetMinute(uint8_t minute);
void DS1307_SetHour(uint8_t hour_24mode);
void DS1307_SetYear(uint8_t year);
void DS1307_SetMonth(uint8_t month);
void DS1307_SetDate(uint8_t date);
void DS1307_SetDayOfWeek(uint8_t dayOfWeek);

#endif

