/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdbool.h>
#include "lcd.h"
#include "ds1307.h"
//#include "LCD1602.h"
//#include "lcd16x2.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

#define BUZZER_ON			HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET)
#define BUZZER_OFF		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET)

typedef enum {
	MODE_DISPLAY_TIME = 0,
	MODE_SETUP_TIME = 1,
	MODE_SETUP_ALARM = 2,
	MODE_COUNT = 3,
} mode_t;

typedef enum {
	BUTTON_CONG = 0,
	BUTTON_OK = 1,
	BUTTON_TRU = 2,
} button_t;

ds1307_t ds1307;
mode_t mode = MODE_DISPLAY_TIME;
mode_t screen = MODE_DISPLAY_TIME;
button_t button = BUTTON_CONG;

uint8_t hour_setup = 0;
uint8_t min_setup = 0;
uint8_t sec_setup = 0;

bool set_hour = false;
bool set_min = false;
bool set_sec = false;

uint8_t hour_alarm = 0;
uint8_t min_alarm = 0;
uint8_t sec_alarm = 0;

bool set_hour_alarm = false;
bool set_min_alarm = false;
bool set_sec_alarm = false;
bool set_alarm = false;

uint32_t start_btn_0, end_btn_0;
uint32_t start_btn_1, end_btn_1;
uint32_t start_btn_2, end_btn_2;

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_0)  // +
	{
		if(HAL_GPIO_ReadPin(BTN0_GPIO_Port, BTN0_Pin) == 0)
		{
			start_btn_0 = HAL_GetTick();
			return;
		} else {
			end_btn_0 = HAL_GetTick();
		}
		if((end_btn_0 > start_btn_0) && (end_btn_0 - start_btn_0 > 100)) // real press
		{
			button = BUTTON_CONG;
			if(mode == MODE_DISPLAY_TIME) {
				screen = (screen + 1) % MODE_COUNT;
			}
			
			if(mode == MODE_SETUP_TIME) {
				if(!set_hour) {
					hour_setup = (hour_setup+1) % 24;
				}
				else if(!set_min) {
					min_setup = (min_setup+1) % 60;
				}
				else if(!set_sec) {
					sec_setup = (sec_setup+1) % 60;
				}
			}
			
			if(mode == MODE_SETUP_ALARM) {
				if(!set_hour_alarm) {
					hour_alarm = (hour_alarm+1) % 24;
				}
				else if(!set_min_alarm) {
					min_alarm = (min_alarm+1) % 60;
				}
				else if(!set_sec_alarm) {
					sec_alarm = (sec_alarm+1) % 60;
				}
			}
		}	
	}
	else if (GPIO_Pin == GPIO_PIN_1) // OK
	{
		if(HAL_GPIO_ReadPin(BTN1_GPIO_Port, BTN1_Pin) == 0)
		{
			start_btn_1 = HAL_GetTick();			
			return;
		} else {
			end_btn_1 = HAL_GetTick();
		}
		if((end_btn_1 > start_btn_1) && (end_btn_1 - start_btn_1 > 100)) // noise
		{
			button = BUTTON_OK;
			if(mode != screen) {
				mode = screen;
				hour_setup = min_setup = sec_setup = 0;
				set_hour = set_min = set_sec = false;
				
				hour_alarm = min_alarm = sec_alarm = 0;
				set_hour_alarm = set_min_alarm = set_sec_alarm = set_alarm = false;
			}
			else if(mode == MODE_SETUP_TIME) {
				if(!set_hour)
					set_hour = true;
				else if(!set_min)
					set_min = true;
				else if(!set_sec)
					set_sec = true;		
				
				if(set_hour && set_min && set_sec) {
					mode = screen = MODE_DISPLAY_TIME;
				}
			}
			else if(mode == MODE_SETUP_ALARM) {
				if(!set_hour_alarm)
					set_hour_alarm = true;
				else if(!set_min_alarm)
					set_min_alarm = true;
				else if(!set_sec_alarm)
					set_sec_alarm = true;		
				
				if(set_hour_alarm && set_min_alarm && set_sec_alarm) {
					mode = screen = MODE_DISPLAY_TIME;
					set_alarm = true;
				}
			}		
		}
	}
	else if (GPIO_Pin == GPIO_PIN_2) // -
	{
		if(HAL_GPIO_ReadPin(BTN2_GPIO_Port, BTN2_Pin) == 0)
		{
			start_btn_2 = HAL_GetTick();
			return;
		} else {
			end_btn_2 = HAL_GetTick();
		}
		if((end_btn_2 > start_btn_2) && (end_btn_2 - start_btn_2 > 100)) // noise
		{
			button = BUTTON_TRU;
			if(mode == MODE_DISPLAY_TIME) {
				if(screen > 0) {
					screen = screen - 1;
				} else {
					screen = MODE_SETUP_ALARM;
				}
			}
			
			if(mode == MODE_SETUP_TIME) {
				if(!set_hour) {
					if(hour_setup > 0) {
						hour_setup = (hour_setup-1) % 24;
					} else {
						hour_setup = 23;
					}
				}
				else if(!set_min) {
					if(min_setup > 0) {
						min_setup = (min_setup-1) % 60;
					} else {
						min_setup = 59;
					}
				}
				else if(!set_sec) {
					if(sec_setup > 0) {
						sec_setup = (sec_setup-1) % 60;
					} else {
						sec_setup = 59;
					}
				}
			}
			
			if(mode == MODE_SETUP_ALARM) {
				if(!set_hour_alarm) {
					if(hour_alarm > 0) {
						hour_alarm = (hour_alarm-1) % 24;
					} else {
						hour_alarm = 23;
					}
				}
				else if(!set_min_alarm) {
					if(min_alarm > 0) {
						min_alarm = (min_alarm-1) % 60;
					} else {
						min_alarm = 59;
					}
				}
				else if(!set_sec_alarm) {
					if(sec_alarm > 0) {
						sec_alarm = (sec_alarm-1) % 60;
					} else {
						sec_alarm = 59;
					}
				}
			}			
		}
		
		
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
	HAL_TIM_Base_Start(&htim2);
	DS1307_Init();
	lcd_init_4bit();
	
	char time_str[30];
	uint32_t start_buzzer = 0;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		switch (screen)
		{
			case MODE_DISPLAY_TIME:
				if(!set_hour) {
					lcd_clear();
					lcd_put_cur(0,0);
					lcd_print_str("Mode 0");
					lcd_put_cur(1,0);
					ds1307.hour = DS1307_GetHour();
					ds1307.min = DS1307_GetMinute();
					ds1307.sec = DS1307_GetSecond();
					snprintf(time_str, 30, "%02d:%02d:%02d", ds1307.hour, ds1307.min, ds1307.sec);
					lcd_print_str(time_str);
					
					if(set_alarm) {
						if(ds1307.hour == hour_alarm && ds1307.min == min_alarm && ds1307.sec == sec_alarm)
						{
							BUZZER_ON;
							start_buzzer = HAL_GetTick();
						}
					}
					
					if(start_buzzer) {
						if(HAL_GetTick() - start_buzzer > 10000) {
							BUZZER_OFF;
							start_buzzer = 0;
						}
					}
					
					HAL_Delay(1000);
				} else {
					DS1307_SetClockHalt(0);
					DS1307_SetHour(hour_setup);
					DS1307_SetMinute(min_setup);
					DS1307_SetSecond(sec_setup);
					set_hour = set_min = set_sec = false;
				}
				break;
			case MODE_SETUP_TIME:
				lcd_clear();
				lcd_put_cur(0,0);
				lcd_print_str("Mode 1");
				lcd_put_cur(1,0);
				// 00:00:00
				snprintf(time_str, 30, "%02d:%02d:%02d", hour_setup, min_setup, sec_setup);
				lcd_print_str(time_str);
				HAL_Delay(100);
				break;
			case MODE_SETUP_ALARM:
				lcd_clear();
				lcd_put_cur(0,0);
				lcd_print_str("Mode 2");
				lcd_put_cur(1,0);
				// 00:00:00
				snprintf(time_str, 30, "%02d:%02d:%02d", hour_alarm, min_alarm, sec_alarm);
				lcd_print_str(time_str);
				HAL_Delay(100);			
				break;			
			default:
				break;
		}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 71;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 65535;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LED_Pin|RS_Pin|E_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, D4_Pin|D5_Pin|D6_Pin|D7_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : BTN0_Pin BTN1_Pin BTN2_Pin */
  GPIO_InitStruct.Pin = BTN0_Pin|BTN1_Pin|BTN2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_Pin RS_Pin E_Pin */
  GPIO_InitStruct.Pin = LED_Pin|RS_Pin|E_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : D4_Pin D5_Pin D6_Pin D7_Pin */
  GPIO_InitStruct.Pin = D4_Pin|D5_Pin|D6_Pin|D7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI1_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI2_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
