#include "main.h"
#include "uart.h"
#include "ringbuf.h"

UART_HandleTypeDef huart1;
uint8_t ch;
extern RINGBUF ring;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
		if(huart->Instance == USART1)
		{
			RINGBUF_Put(&ring, ch);
			HAL_UART_Receive_IT(&huart1, &ch, 1);
		}
}


void uart_init(int baudrate)
{
  huart1.Instance = USART1;
  huart1.Init.BaudRate = baudrate;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
	HAL_UART_Receive_IT(&huart1, &ch, 1);
}

void uart_send(uint8_t *data, uint16_t length)
{
	HAL_UART_Transmit(&huart1, data, length, 1000);
}