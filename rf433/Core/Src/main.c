/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
typedef enum {
	MODE_LEARNING = 0,
	MODE_CONTROL = 1
} mode_t;

typedef enum {
	START_RF,
	LEARNING_RF,
	DONE_RF
} state_rf_t;

uint16_t arr_rf433[100];
uint16_t arr_rf433_index = 0;

uint8_t learn_done = 0;

mode_t mode = MODE_LEARNING;
state_rf_t state_rf = START_RF;


uint32_t btn_cmd_1 = 0;
uint32_t btn_cmd_2 = 0;
uint32_t btn_cmd_3 = 0;
uint32_t btn_cmd_4 = 0;

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_0) {
		uint16_t cnt = htim2.Instance->CNT;
		htim2.Instance->CNT = 0;
		if(state_rf == START_RF && cnt > 8000) {
			state_rf = LEARNING_RF;
		}
		else if(state_rf == LEARNING_RF && cnt > 200 && cnt < 1500) {
			arr_rf433[arr_rf433_index] = cnt;
			arr_rf433_index++;
		}
		else if(state_rf == LEARNING_RF && cnt > 8000) {
			state_rf = DONE_RF;
		}
		else if(state_rf == LEARNING_RF){
			memset(arr_rf433, 0, sizeof(arr_rf433));
			arr_rf433_index = 0;
			state_rf = START_RF;
		}
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
	htim2.Instance->CNT = 0;
	HAL_TIM_Base_Start(&htim2);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
		if(mode == MODE_LEARNING) {
			if(state_rf == DONE_RF) {
				if(btn_cmd_1 == 0) {
					for(uint8_t i=0;i<24;i++) {
						if(arr_rf433[2*i] > arr_rf433[2*i+1]) {
							btn_cmd_1 = btn_cmd_1 | (1<<(23-i));
						}
					}
				}
				else if(btn_cmd_2 == 0) {
					for(uint8_t i=0;i<24;i++) {
						if(arr_rf433[2*i] > arr_rf433[2*i+1]) {
							btn_cmd_2 = btn_cmd_2 | (1<<(23-i));
						}
					}	
				}
				else if(btn_cmd_3 == 0) {
					for(uint8_t i=0;i<24;i++) {
						if(arr_rf433[2*i] > arr_rf433[2*i+1]) {
							btn_cmd_3 = btn_cmd_3 | (1<<(23-i));
						}
					}					
				}
				else if(btn_cmd_4 == 0) {
					for(uint8_t i=0;i<24;i++) {
						if(arr_rf433[2*i] > arr_rf433[2*i+1]) {
							btn_cmd_4 = btn_cmd_4 | (1<<(23-i));
						}
					}					
				}
				if(btn_cmd_4 != 0) {
					mode = MODE_CONTROL;
				}
				HAL_Delay(1000);
				memset(arr_rf433, 0, sizeof(arr_rf433));
				arr_rf433_index = 0;
				state_rf = START_RF;
			}
		}
		else if(mode == MODE_CONTROL) {
			if(state_rf == DONE_RF) {
				uint32_t new_cmd = 0;
				for(uint8_t i=0;i<24;i++) {
					if(arr_rf433[2*i] > arr_rf433[2*i+1]) {
						new_cmd = new_cmd | (1<<(23-i));
					}
				}
				
				if(new_cmd == btn_cmd_1) {
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, 0);// on
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, 1);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 1);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, 1);
				}
				else if(new_cmd == btn_cmd_2) {
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, 1);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, 0);// on
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 1);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, 1);					
				} 
				else if(new_cmd == btn_cmd_3) {
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, 1);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, 1);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 0);// on
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, 1);							
				}
				else if(new_cmd == btn_cmd_4) {
					HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, 1);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, 1);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, 1);
					HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, 0);		// on					
				}
				
				memset(arr_rf433, 0, sizeof(arr_rf433));
				arr_rf433_index = 0;
				state_rf = START_RF;
			}
		}
		
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 71;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 65535;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4, GPIO_PIN_SET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA2 PA3 PA4 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
