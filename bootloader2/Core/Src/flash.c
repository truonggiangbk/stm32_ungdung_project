#include "main.h"
#include "flash.h"

// int, float, struct, array, string

void Flash_Erase(uint32_t address)
{
	HAL_FLASH_Unlock();
	
	FLASH_EraseInitTypeDef pEraseInit;
	pEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;
	pEraseInit.Banks = 1;
	pEraseInit.PageAddress = address;
	pEraseInit.NbPages = 8;
	uint32_t PageError;
	
	HAL_FLASHEx_Erase(&pEraseInit, &PageError);
	HAL_FLASH_Lock();
}

void Flash_Write_Int(uint32_t address, int value)
{
	HAL_FLASH_Unlock();
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, address, value);
	HAL_FLASH_Lock();
}

int Flash_Read_Int(uint32_t address)
{
	return *(__IO uint16_t *)address;
}

void Flash_Write_Float(uint32_t address, float value)
{
	HAL_FLASH_Unlock();
	uint8_t bytes[4];  // bytes[0] bytes[1] bytes[2] bytes[3]
	*(float*) bytes = value;
	HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address, *(uint32_t*)bytes);
	HAL_FLASH_Lock();
}

float Flash_Read_Float(uint32_t address)
{
	uint32_t value = *(__IO uint32_t *)address;
	return *(float *)&value;
}

//0x01,0x02,0x03,0x04, 0x05
void Flash_Write_Array(uint32_t address, uint8_t *data, uint16_t len)
{
	HAL_FLASH_Unlock();
	for(uint16_t i=0;i<(len+1)/2;i++) 
	{
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD, address + i*2, *(uint16_t*)&data[i*2]);
	}
	HAL_FLASH_Lock();
}

void Flash_Read_Array(uint32_t address, uint8_t *data, uint16_t len)
{
	uint16_t *pt = (uint16_t*)data;
	for(uint16_t i=0;i<(len+1)/2;i++) 
	{
		*pt = *(__IO uint16_t *)(address + 2*i);
		pt++;
	}
}

void Flash_Write_Struct(uint32_t address, wifi_info_t data)
{
	Flash_Write_Array(address, (uint8_t*)&data, sizeof(wifi_info_t));
}

void Flash_Read_Struct(uint32_t address, wifi_info_t *data)
{
	Flash_Read_Array(address, (uint8_t*)data, sizeof(wifi_info_t));
}
