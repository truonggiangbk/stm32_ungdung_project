#ifndef UART_H
#define UART_H
#include <stdint.h>

void uart_init(int baudrate);
void uart_send(uint8_t *data, uint16_t length);

#endif