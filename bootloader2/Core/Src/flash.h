#ifndef __FLASH_H
#define __FLASH_H
#include <stdint.h>

#pragma pack(1) 
typedef struct {
	uint8_t no;
	uint16_t number;
	char ssid[20];
	char pass[20];
} wifi_info_t;
#pragma pack()

void Flash_Erase(uint32_t address);
void Flash_Write_Int(uint32_t address, int value);
int Flash_Read_Int(uint32_t address);
void Flash_Write_Float(uint32_t address, float value);
float Flash_Read_Float(uint32_t address);
void Flash_Write_Array(uint32_t address, uint8_t *data, uint16_t len);
void Flash_Read_Array(uint32_t address, uint8_t *data, uint16_t len);
void Flash_Write_Struct(uint32_t address, wifi_info_t data);
void Flash_Read_Struct(uint32_t address, wifi_info_t *data);
#endif