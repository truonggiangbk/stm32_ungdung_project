# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "D:/esp501/Espressif/frameworks/esp-idf-v5.0.1/components/bootloader/subproject"
  "F:/stm32_full/project/esp_http_client/build/bootloader"
  "F:/stm32_full/project/esp_http_client/build/bootloader-prefix"
  "F:/stm32_full/project/esp_http_client/build/bootloader-prefix/tmp"
  "F:/stm32_full/project/esp_http_client/build/bootloader-prefix/src/bootloader-stamp"
  "F:/stm32_full/project/esp_http_client/build/bootloader-prefix/src"
  "F:/stm32_full/project/esp_http_client/build/bootloader-prefix/src/bootloader-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "F:/stm32_full/project/esp_http_client/build/bootloader-prefix/src/bootloader-stamp/${subDir}")
endforeach()
if(cfgdir)
  file(MAKE_DIRECTORY "F:/stm32_full/project/esp_http_client/build/bootloader-prefix/src/bootloader-stamp${cfgdir}") # cfgdir has leading slash
endif()
