/* ESP HTTP Client Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "esp_event.h"
#include "esp_netif.h"
#include "protocol_examples_common.h"
#include "esp_tls.h"
#if CONFIG_MBEDTLS_CERTIFICATE_BUNDLE
#include "esp_crt_bundle.h"
#endif

#include "esp_http_client.h"
#include "min.h"
#include "dev_uart.h"
#include "dev_gpio.h"

#define MAX_HTTP_RECV_BUFFER 512
#define MAX_HTTP_OUTPUT_BUFFER 2048
static const char *TAG = "HTTP_CLIENT";

struct min_context min;

typedef enum {
    MIN_ID_START_DOWNLOAD,
    MIN_ID_DOWNLOADING,
    MIN_ID_FINISH
} min_id_download_state_t;

void min_tx_byte(uint8_t port, uint8_t byte)
{
	uart_send(&byte, 1);
}

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    static char *output_buffer;  // Buffer to store response of http request from event handler
    static int output_len;       // Stores number of bytes read
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
            break;
        case HTTP_EVENT_ON_DATA:
        {
            ESP_LOGI(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            uint16_t length = evt->data_len;
            uint8_t *data = (uint8_t *)evt->data;
            while (length >= 128) {
                min_send_frame(&min, MIN_ID_DOWNLOADING, data, 128);
                printf("->\n");
                vTaskDelay(5 / portTICK_PERIOD_MS);
                data += 128;
                length -= 128;
            }
            
            if(length > 0) {
                min_send_frame(&min, MIN_ID_DOWNLOADING, data, length);
                printf("->\n");
                vTaskDelay(5 / portTICK_PERIOD_MS);
            }

            /*
             *  Check for chunked encoding is added as the URL for chunked encoding used in this example returns binary data.
             *  However, event handler can also be used in case chunked encoding is used.
             */
            // if (!esp_http_client_is_chunked_response(evt->client)) {
            //     // If user_data buffer is configured, copy the response into the buffer
            //     if (evt->user_data) {
            //         memcpy(evt->user_data + output_len, evt->data, evt->data_len);
            //     } else {
            //         if (output_buffer == NULL) {
            //             output_buffer = (char *) malloc(esp_http_client_get_content_length(evt->client));
            //             output_len = 0;
            //             if (output_buffer == NULL) {
            //                 ESP_LOGE(TAG, "Failed to allocate memory for output buffer");
            //                 return ESP_FAIL;
            //             }
            //         }
            //         memcpy(output_buffer + output_len, evt->data, evt->data_len);
            //     }
            //     output_len += evt->data_len;
            // }

        }    break;
        case HTTP_EVENT_ON_FINISH:
        {
            ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
            uint8_t data[1] = {0x00};
            min_send_frame(&min, MIN_ID_FINISH, data, 1);
            if (output_buffer != NULL) {
                // Response is accumulated in output_buffer. Uncomment the below line to print the accumulated response
                // ESP_LOG_BUFFER_HEX(TAG, output_buffer, output_len);
                free(output_buffer);
                output_buffer = NULL;
            }
            output_len = 0;
        }   break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
            int mbedtls_err = 0;
            esp_err_t err = esp_tls_get_and_clear_last_error((esp_tls_error_handle_t)evt->data, &mbedtls_err, NULL);
            if (err != 0) {
                ESP_LOGI(TAG, "Last esp error code: 0x%x", err);
                ESP_LOGI(TAG, "Last mbedtls failure: 0x%x", mbedtls_err);
            }
            if (output_buffer != NULL) {
                free(output_buffer);
                output_buffer = NULL;
            }
            output_len = 0;
            break;
        case HTTP_EVENT_REDIRECT:
            ESP_LOGD(TAG, "HTTP_EVENT_REDIRECT");
            esp_http_client_set_header(evt->client, "From", "user@example.com");
            esp_http_client_set_header(evt->client, "Accept", "text/html");
            esp_http_client_set_redirection(evt->client);
            break;
    }
    return ESP_OK;
}

static void http_download_chunk(void)
{
    esp_http_client_config_t config = {
        .url = "http://192.168.0.111/app2.bin",
        .event_handler = _http_event_handler,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);

    if (err == ESP_OK) {
        ESP_LOGI(TAG, "HTTP chunk encoding Status = %d, content_length = %lld",
                esp_http_client_get_status_code(client),
                esp_http_client_get_content_length(client));
    } else {
        ESP_LOGE(TAG, "Error perform http request %s", esp_err_to_name(err));
    }
    esp_http_client_cleanup(client);
}

static void http_test_task(void *pvParameters)
{
    http_download_chunk();
    ESP_LOGI(TAG, "Finish http example");
    vTaskDelete(NULL);
}

void button_callback(int pin) {
    uint8_t data[] = {0x00};
    min_send_frame(&min, MIN_ID_START_DOWNLOAD, data, 1);
}

void uart_callback(uint8_t *data, uint16_t length) {
    min_poll(&min, data, length);
}

void min_application_handler(uint8_t min_id, uint8_t const *min_payload, uint8_t len_payload, uint8_t port)
{
    switch (min_id)
    {
    case MIN_ID_START_DOWNLOAD:
        if (*min_payload == 0x00) {
            printf("STM32 erase flash done\n");
            printf("Start upload firmware to STM32\n");
            xTaskCreate(&http_test_task, "http_test_task", 8192, NULL, 5, NULL);
        } 
        break;
    
    default:
        break;
    }
}

void app_main(void)
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());

    min_init_context(&min, 0);
    uart_init(115200);
    uart_set_callback(uart_callback);

    gpio_input_init(0);
    gpio_set_callback(button_callback);
    /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
     * Read "Establishing Wi-Fi or Ethernet Connection" section in
     * examples/protocols/README.md for more information about this function.
     */
    ESP_ERROR_CHECK(example_connect());
    ESP_LOGI(TAG, "Connected to AP, begin http example");    

    // xTaskCreate(&http_test_task, "http_test_task", 8192, NULL, 5, NULL);
}
