#ifndef _DEV_UART_H
#define _DEV_UART_H
#include <stdint.h>

typedef void (*uart_data_handle_t) (uint8_t *data, uint16_t length);

void uart_init(int baudrate);
void uart_send(uint8_t *data, uint16_t length);
void uart_set_callback(void *cb);
#endif