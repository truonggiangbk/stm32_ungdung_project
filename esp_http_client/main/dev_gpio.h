#ifndef __DEV_GPIO_H
#define __DEV_GPIO_H

typedef void (*dev_gpio_handle_t) (int gpio);

void gpio_input_init(int pin);
void gpio_set_callback(void *cb);
#endif